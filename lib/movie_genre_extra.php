<?php


  /****************************************************************
   * Class: movie_genre_extra
   * Author: Ian Bezanson
   * Date: 2010-08-06
   *
   * The purpose of this class is to extend the base class for
   * user defined functions, properties and methods.
   ***************************************************************/


  include_once('movie_genre.php');


  class movie_genre_extra extends movie_genre {

    /*****************************************************
     * Begin find_by_genre
     ****************************************************/
    // Function finds item from movie_genre based on the primary key
    public function find_by_genre() {

      $sql = 'SELECT movie_genre_id';
      $sql .= ', movie_genre';
      $sql .= ', description';
      $sql .= ', active';
      $sql .= ', created';
      $sql .= ', updated';
      $sql .= ' FROM {movie_genre}';
      $sql .= ' WHERE LOWER(movie_genre) LIKE LOWER(\'%s%%\')';

      $result = db_query(
        $sql,
        $this->movie_genre
      );

      if( $row = db_fetch_object($result) ) {
        $this->fill_object($row);
        return true;
      }
      else {
        return false;
      }
    }
    /*****************************************************
     * End find_by_genre
     ****************************************************/

  };
?>