<?php


  /****************************************************************
   * Class: movie_format_extra
   * Author: Ian Bezanson
   * Date: 2010-08-06
   *
   * The purpose of this class is to extend the base class for
   * user defined functions, properties and methods.
   ***************************************************************/


  include_once('movie_format.php');


  class movie_format_extra extends movie_format {

    /*****************************************************
     * Begin find_by_format
     ****************************************************/
    // Function finds item from movie_format based on the primary key
    public function find_by_format() {

      $sql = 'SELECT movie_format_id';
      $sql .= ', movie_format';
      $sql .= ', description';
      $sql .= ', active';
      $sql .= ', created';
      $sql .= ', updated';
      $sql .= ' FROM {movie_format}';
      $sql .= ' WHERE LOWER(movie_format) LIKE LOWER(\'%s%%\')';

      $result = db_query(
        $sql,
        $this->movie_format
      );

      if( $row = db_fetch_object($result) ) {
        $this->fill_object($row);
        return true;
      }
      else {
        return false;
      }
    }
    /*****************************************************
     * End find_by_format
     ****************************************************/

  };
?>