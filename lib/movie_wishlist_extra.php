<?php


  /****************************************************************
   * Class: movie_wishlist_extra
   * Author: Ian Bezanson
   * Date: 2010-08-06
   *
   * The purpose of this class is to extend the base class for
   * user defined functions, properties and methods.
   ***************************************************************/


  include_once('movie_wishlist.php');


  class movie_wishlist_extra extends movie_wishlist {

    /*****************************************************
     * Begin find_by_uid
     ****************************************************/
    // Function finds item from movie based on the user id
    public function find_by_uid() {

      $sql = 'SELECT movie_wishlist_id';
      $sql .= ', movie_format_id';
      $sql .= ', uid';
      $sql .= ', imdb_id';
      $sql .= ', title';
      $sql .= ', release_date';
      $sql .= ', runtime';
      $sql .= ', added_to_movies';
      $sql .= ', active';
      $sql .= ', created';
      $sql .= ', updated';
      $sql .= ' FROM {movie_wishlist}';
      $sql .= ' WHERE uid = %d';
      $sql .= ' AND active';
      $result = db_query($sql, $this->uid);

      $return_data = array();
      while( $row = db_fetch_object($result) ) {
        $item = new movie_extra();
        $item->fill_object($row);
        $return_data[] = $item;
      }
      return $return_data;
    }
    /*****************************************************
     * End find_by_uid
     ****************************************************/


    /*****************************************************
     * Begin find_by_uid_paginated
     ****************************************************/
    // Finds all records in table or records matching passed where clause
    public function find_by_uid_paginated($sorted_header) {

      $sql = 'SELECT mw.movie_wishlist_id';
      $sql .= ', mw.movie_format_id';
      $sql .= ', mw.uid';
      $sql .= ', mw.imdb_id';
      $sql .= ', mw.title';
      $sql .= ', mw.release_date';
      $sql .= ', mw.runtime';
      $sql .= ', mw.added_to_movies';
      $sql .= ', mw.active';
      $sql .= ', mw.created';
      $sql .= ', mw.updated';
      $sql .= ', mf.movie_format';
      $sql .= ' FROM {movie_wishlist} mw';
      $sql .= ' LEFT JOIN {movie_format} mf ON mw.movie_format_id = mf.movie_format_id';
      $sql .= ' WHERE uid = ' . $this->uid;

      $result_count = '
        SELECT
          count(movie_wishlist_id) as this_count
        FROM
        (
          ' . $sql . '
        ) count_table
      ';

      $result = pager_query
      (
        db_rewrite_sql
        ('
          ' . $sql . '
          ' . $sorted_header
          , '{movie_wishlist}'
          , 'movie_wishlist_id'
        ),
        variable_get('movie_paginate', 20),
        0,
        $result_count
      );
      return $result;
    }
    /*****************************************************
     * End find_by_uid_paginated
     ****************************************************/


    /*****************************************************
     * Begin delete_by_uid
     ****************************************************/
    // Function deletes item(s) from movie_wishlist based on the user id
    public function delete_by_uid() {

      $sql = 'DELETE FROM {movie_wishlist} WHERE uid = %d';
      $result = db_query($sql, $this->uid);

    }
    /*****************************************************
     * End delete_by_uid
     ****************************************************/

  };
?>