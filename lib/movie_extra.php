<?php


  /****************************************************************
   * Class: movie_extra
   * Author: Ian Bezanson
   * Date: 2010-08-06
   *
   * The purpose of this class is to extend the base class for
   * user defined functions, properties and methods.
   ***************************************************************/


  include_once('movie.php');


  class movie_extra extends movie {

    /*****************************************************
     * Begin find_by_uid
     ****************************************************/
    // Function finds item from movie based on the user id
    public function find_by_uid() {

      $sql = 'SELECT movie_id';
      $sql .= ', movie_format_id';
      $sql .= ', uid';
      $sql .= ', imdb_id';
      $sql .= ', title';
      $sql .= ', release_date';
      $sql .= ', runtime';
      $sql .= ', active';
      $sql .= ', created';
      $sql .= ', updated';
      $sql .= ' FROM {movie}';
      $sql .= ' WHERE uid = %d';
      $result = db_query($sql, $this->uid);

      $return_data = array();
      while( $row = db_fetch_object($result) ) {
        $item = new movie_extra();
        $item->fill_object($row);
        $return_data[] = $item;
      }
      return $return_data;
    }
    /*****************************************************
     * End find_by_uid
     ****************************************************/


    /*****************************************************
     * Begin find_by_uid_paginated
     ****************************************************/
    // Finds all records in table or records matching passed where clause
    public function find_by_uid_paginated($sorted_header) {

      $sql = 'SELECT m.movie_id';
      $sql .= ', m.movie_format_id';
      $sql .= ', m.uid';
      $sql .= ', m.imdb_id';
      $sql .= ', m.title';
      $sql .= ', m.release_date';
      $sql .= ', m.runtime';
      $sql .= ', m.active';
      $sql .= ', m.created';
      $sql .= ', m.updated';
      $sql .= ', mf.movie_format';
      $sql .= ' FROM {movie} m';
      $sql .= ' LEFT JOIN {movie_format} mf ON m.movie_format_id = mf.movie_format_id';
      $sql .= ' WHERE uid = ' . $this->uid;

      $result_count = '
        SELECT
          count(movie_id) as this_count
        FROM
        (
          ' . $sql . '
        ) count_table
      ';

      $result = pager_query
      (
        db_rewrite_sql
        ('
          ' . $sql . '
          ' . $sorted_header
          , '{movie}'
          , 'movie_id'
        ),
        variable_get('movie_paginate', 20),
        0,
        $result_count
      );
      return $result;
    }
    /*****************************************************
     * End find_by_uid_paginated
     ****************************************************/


    /*****************************************************
     * Begin delete_by_uid
     ****************************************************/
    // Function deletes item(s) from movie based on the user id
    public function delete_by_uid() {

      $sql = 'DELETE FROM {movie} WHERE uid = %d';
      $result = db_query($sql, $this->uid);

    }
    /*****************************************************
     * End delete_by_uid
     ****************************************************/


    /*****************************************************
     * Begin find_conflicts_paginated
     ****************************************************/
    // Finds all records in table or records matching passed where clause
    public function find_conflicts_paginated($sorted_header) {

      $sql = 'SELECT m.movie_id';
      $sql .= ', mw.movie_wishlist_id';
      $sql .= ', m.movie_format_id';
      $sql .= ', m.uid';
      $sql .= ', m.imdb_id';
      $sql .= ', m.title';
      $sql .= ', m.release_date';
      $sql .= ', m.runtime';
      $sql .= ', m.active';
      $sql .= ', m.created';
      $sql .= ', m.updated';
      $sql .= ' FROM {movie} m';
      $sql .= ' JOIN {movie_wishlist} mw on m.title = mw.title';
      $sql .= ' AND m.release_date = mw.release_date';
      $sql .= ' AND m.uid = mw.uid';
      $sql .= ' WHERE m.uid = ' . $this->uid;
      $sql .= ' AND m.active';
      $sql .= ' AND mw.active';
      $sql .= ' AND !mw.added_to_movies';

      $result_count = '
        SELECT
          count(movie_id) as this_count
        FROM
        (
          ' . $sql . '
        ) count_table
      ';

      $result = pager_query
      (
        db_rewrite_sql
        ('
          ' . $sql . '
          ' . $sorted_header
          , '{movie}'
          , 'movie_id'
        ),
        variable_get('movie_paginate', 20),
        0,
        $result_count
      );
      return $result;
    }
    /*****************************************************
     * End find_conflicts_paginated
     ****************************************************/

  };
?>