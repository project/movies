<?php

/**********************************************************************
 * Class: movie_wishlist_genre_link
 * Author: Ian Bezanson
 * Date: 2010-08-06
 *********************************************************************/

class movie_wishlist_genre_link {

  var $movie_wishlist_genre_link_id;
  var $movie_wishlist_id;
  var $movie_genre_id;
  var $active;
  var $created;
  var $updated;

  /*****************************************************
   * Begin constructor
   ****************************************************/
  // Constructor
  public function __construct() {

    // Initialize class properties
    // useful for when update or insert is called
    // and all values are not set
    $this->movie_wishlist_genre_link_id = 0;
    $this->movie_wishlist_id = 0;
    $this->movie_genre_id = 0;
    $this->active = 0;
    $this->created = 0;
    $this->updated = 0;
  }
  /*****************************************************
   * End constructor
   ****************************************************/

  /*****************************************************
   * Begin fill
   ****************************************************/
  // Populates a class object with values from another class or an array
  protected function fill_object($obj_data) {

    if( gettype($obj_data) == 'object' ) {

      // Object is another instance of class, assign values accordingly
      $this->movie_wishlist_genre_link_id = $obj_data->movie_wishlist_genre_link_id;
      $this->movie_wishlist_id = $obj_data->movie_wishlist_id;
      $this->movie_genre_id = $obj_data->movie_genre_id;
      $this->active = $obj_data->active;
      $this->created = $obj_data->created;
      $this->updated = $obj_data->updated;
    }
    else {
      $this->movie_wishlist_genre_link_id = $obj_data['movie_wishlist_genre_link_id'];
      $this->movie_wishlist_id = $obj_data['movie_wishlist_id'];
      $this->movie_genre_id = $obj_data['movie_genre_id'];
      $this->active = $obj_data['active'];
      $this->created = $obj_data['created'];
      $this->updated = $obj_data['updated'];
    }
  }
  /*****************************************************
   * End fill
   ****************************************************/

  /*****************************************************
   * Begin find
   ****************************************************/
  // Function finds item from movie_wishlist_genre_link based on the primary key
  public function find() {

    $sql = 'SELECT movie_wishlist_genre_link_id';
    $sql .= ', movie_wishlist_id';
    $sql .= ', movie_genre_id';
    $sql .= ', active';
    $sql .= ', created';
    $sql .= ', updated';
    $sql .= ' FROM {movie_wishlist_genre_link}';
    $sql .= ' WHERE movie_wishlist_genre_link_id = %d';
    $result = db_query($sql, $this->movie_wishlist_genre_link_id);

    if( $row = db_fetch_object($result) ) {
      $this->fill_object($row);
      return true;
    }
    else {
      return false;
    }
  }
  /*****************************************************
   * End find
   ****************************************************/

  /*****************************************************
   * Begin find_many
   ****************************************************/
  // Finds all records in table or records matching passed where clause
  public function find_many($sql_ext = '', $order_ext = '') {

    $sql = 'SELECT movie_wishlist_genre_link_id';
    $sql .= ', movie_wishlist_id';
    $sql .= ', movie_genre_id';
    $sql .= ', active';
    $sql .= ', created';
    $sql .= ', updated';
    $sql .= ' FROM {movie_wishlist_genre_link}';

    if( $sql_ext != '' ) {
      $sql .= ' WHERE ' . $sql_ext;
    }

    if( $order_ext != '' ) {
      $sql .= ' ' . $order_ext;
    }

    $result = db_query($sql);

    $return_data = array();
    while( $row = db_fetch_object($result) ) {
      $item = new movie_wishlist_genre_link_extra();
      $item->fill_object($row);
      $return_data[] = $item;
    }
    return $return_data;
  }
  /*****************************************************
   * End find_many
   ****************************************************/

  /*****************************************************
   * Begin find_many_paginated
   ****************************************************/
  // Finds all records in table or records matching passed where clause
  public function find_many_paginated($sorted_header, $sql_ext = '', $order_ext = '') {

    $sql = 'SELECT movie_wishlist_genre_link_id';
    $sql .= ', movie_wishlist_id';
    $sql .= ', movie_genre_id';
    $sql .= ', active';
    $sql .= ', created';
    $sql .= ', updated';
    $sql .= ' FROM {movie_wishlist_genre_link}';

    if( $sql_ext != '' ) {
      $sql .= ' WHERE ' . $sql_ext;
    }

    if( $order_ext != '' ) {
      $sql .= ' ' . $order_ext;
    }

    $result_count = '
      SELECT
        count(movie_wishlist_genre_link_id) as this_count
      FROM
      (
        ' . $sql . '
      ) count_table
    ';

    $result = pager_query
    (
      db_rewrite_sql
      ('
        ' . $sql . '
        ' . $sorted_header
        , '{movie_wishlist_genre_link}'
        , 'movie_wishlist_genre_link_id'
      ),
      variable_get('movie_wishlist_genre_link_paginate', 20),
      0,
      $result_count
    );
    return $result;
  }
  /*****************************************************
   * End find_many_paginated
   ****************************************************/

  /*****************************************************
   * Begin update
   ****************************************************/
  // Updates database with object properties if primary key is set or creates new entry if no primary key exists
  public function update() {

    if( $this->movie_wishlist_genre_link_id == 0 ) {
      $sql = 'INSERT INTO {movie_wishlist_genre_link}';
      $sql .= ' SET ';
      $sql .= 'movie_wishlist_id = %d';
      $sql .= ', movie_genre_id = %d';
      $sql .= ', active = %d';
      $sql .= ', created = %d';
      $sql .= ', updated = %d';
    } else {
      $sql = 'UPDATE {movie_wishlist_genre_link}';
      $sql .= ' SET ';
      $sql .= 'movie_wishlist_id = %d';
      $sql .= ', movie_genre_id = %d';
      $sql .= ', active = %d';
      $sql .= ', created = %d';
      $sql .= ', updated = %d';
      $sql .= ' WHERE movie_wishlist_genre_link_id = %d';
    }
    db_query($sql
      , $this->movie_wishlist_id
      , $this->movie_genre_id
      , $this->active
      , $this->created
      , $this->updated
      , $this->movie_wishlist_genre_link_id
    );

    if( $this->movie_wishlist_genre_link_id == 0 ) {
      $result = db_query('SELECT @@IDENTITY ident');
      $data = db_fetch_object($result);
      $this->movie_wishlist_genre_link_id = $data->ident;
      $this->find();
    }

  }
  /*****************************************************
   * End update
   ****************************************************/

  /*****************************************************
   * Begin delete
   ****************************************************/
  // This function will delete the record from table specified by primary key
  public function delete() {

    $sql = 'DELETE FROM {movie_wishlist_genre_link} WHERE movie_wishlist_genre_link_id = %d';
    db_query($sql, $this->movie_wishlist_genre_link_id);
  }
  /*****************************************************
   * End delete
   ****************************************************/

  /*****************************************************
   * Begin get_num_rows
   ****************************************************/
  // This function will return the number of rows/entries in a table
  public function get_num_rows() {

    $sql = 'SELECT COUNT(movie_wishlist_genre_link_id) as total_entries FROM {movie_wishlist_genre_link}';
    $num_rows = db_fetch_object(db_query($sql));
    return $num_rows->total_entries;
  }
  /*****************************************************
   * End get_num_rows
   ****************************************************/

  /*****************************************************
   * Begin destructor
   ****************************************************/
  public function __destruct() {
    unset(
      $this->movie_wishlist_genre_link_id
      , $this->movie_wishlist_id
      , $this->movie_genre_id
      , $this->active
      , $this->created
      , $this->updated
    );
  }
  /*****************************************************
   * End destructor
   ****************************************************/

}
