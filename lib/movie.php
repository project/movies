<?php

/**********************************************************************
 * Class: movie
 * Author: Ian Bezanson
 * Date: 2010-08-06
 *********************************************************************/

class movie {

  var $movie_id;
  var $movie_format_id;
  var $uid;
  var $imdb_id;
  var $title;
  var $release_date;
  var $runtime;
  var $active;
  var $created;
  var $updated;

  /*****************************************************
   * Begin constructor
   ****************************************************/
  // Constructor
  public function __construct() {

    // Initialize class properties
    // useful for when update or insert is called
    // and all values are not set
    $this->movie_id = 0;
    $this->movie_format_id = 0;
    $this->uid = 0;
    $this->imdb_id = '';
    $this->title = '';
    $this->release_date = 0;
    $this->runtime = 0;
    $this->active = 1;
    $this->created = 0;
    $this->updated = 0;
  }
  /*****************************************************
   * End constructor
   ****************************************************/

  /*****************************************************
   * Begin fill
   ****************************************************/
  // Populates a class object with values from another class or an array
  protected function fill_object($obj_data) {

    if( gettype($obj_data) == 'object' ) {

      // Object is another instance of class, assign values accordingly
      $this->movie_id = $obj_data->movie_id;
      $this->movie_format_id = $obj_data->movie_format_id;
      $this->uid = $obj_data->uid;
      $this->imdb_id = $obj_data->imdb_id;
      $this->title = $obj_data->title;
      $this->release_date = $obj_data->release_date;
      $this->runtime = $obj_data->runtime;
      $this->active = $obj_data->active;
      $this->created = $obj_data->created;
      $this->updated = $obj_data->updated;
    }
    else {
      $this->movie_id = $obj_data['movie_id'];
      $this->movie_format_id = $obj_data['movie_format_id'];
      $this->uid = $obj_data['uid'];
      $this->imdb_id = $obj_data['imdb_id'];
      $this->title = $obj_data['title'];
      $this->release_date = $obj_data['release_date'];
      $this->runtime = $obj_data['runtime'];
      $this->active = $obj_data['active'];
      $this->created = $obj_data['created'];
      $this->updated = $obj_data['updated'];
    }
  }
  /*****************************************************
   * End fill
   ****************************************************/

  /*****************************************************
   * Begin find
   ****************************************************/
  // Function finds item from movie based on the primary key
  public function find() {

    $sql = 'SELECT movie_id';
    $sql .= ', movie_format_id';
    $sql .= ', uid';
    $sql .= ', imdb_id';
    $sql .= ', title';
    $sql .= ', release_date';
    $sql .= ', runtime';
    $sql .= ', active';
    $sql .= ', created';
    $sql .= ', updated';
    $sql .= ' FROM {movie}';
    $sql .= ' WHERE movie_id = %d';
    $result = db_query($sql, $this->movie_id);

    if( $row = db_fetch_object($result) ) {
      $this->fill_object($row);
      return true;
    }
    else {
      return false;
    }
  }
  /*****************************************************
   * End find
   ****************************************************/

  /*****************************************************
   * Begin find_many
   ****************************************************/
  // Finds all records in table or records matching passed where clause
  public function find_many($sql_ext = '', $order_ext = '') {

    $sql = 'SELECT movie_id';
    $sql .= ', movie_format_id';
    $sql .= ', uid';
    $sql .= ', imdb_id';
    $sql .= ', title';
    $sql .= ', release_date';
    $sql .= ', runtime';
    $sql .= ', active';
    $sql .= ', created';
    $sql .= ', updated';
    $sql .= ' FROM {movie}';

    if( $sql_ext != '' ) {
      $sql .= ' WHERE ' . $sql_ext;
    }

    if( $order_ext != '' ) {
      $sql .= ' ' . $order_ext;
    }

    $result = db_query($sql);

    $return_data = array();
    while( $row = db_fetch_object($result) ) {
      $item = new movie_extra();
      $item->fill_object($row);
      $return_data[] = $item;
    }
    return $return_data;
  }
  /*****************************************************
   * End find_many
   ****************************************************/

  /*****************************************************
   * Begin find_many_paginated
   ****************************************************/
  // Finds all records in table or records matching passed where clause
  public function find_many_paginated($sorted_header, $sql_ext = '', $order_ext = '') {

    $sql = 'SELECT movie_id';
    $sql .= ', movie_format_id';
    $sql .= ', uid';
    $sql .= ', imdb_id';
    $sql .= ', title';
    $sql .= ', release_date';
    $sql .= ', runtime';
    $sql .= ', active';
    $sql .= ', created';
    $sql .= ', updated';
    $sql .= ' FROM {movie}';

    if( $sql_ext != '' ) {
      $sql .= ' WHERE ' . $sql_ext;
    }

    if( $order_ext != '' ) {
      $sql .= ' ' . $order_ext;
    }

    $result_count = '
      SELECT
        count(movie_id) as this_count
      FROM
      (
        ' . $sql . '
      ) count_table
    ';

    $result = pager_query
    (
      db_rewrite_sql
      ('
        ' . $sql . '
        ' . $sorted_header
        , '{movie}'
        , 'movie_id'
      ),
      variable_get('movie_paginate', 20),
      0,
      $result_count
    );
    return $result;
  }
  /*****************************************************
   * End find_many_paginated
   ****************************************************/

  /*****************************************************
   * Begin update
   ****************************************************/
  // Updates database with object properties if primary key is set or creates new entry if no primary key exists
  public function update() {

    if( $this->movie_id == 0 ) {
      $sql = 'INSERT INTO {movie}';
      $sql .= ' SET ';
      $sql .= 'movie_format_id = %d';
      $sql .= ', uid = %d';
      $sql .= ', imdb_id = \'%s\'';
      $sql .= ', title = \'%s\'';
      $sql .= ', release_date = %d';
      $sql .= ', runtime = %d';
      $sql .= ', active = %d';
      $sql .= ', created = %d';
      $sql .= ', updated = %d';
    } else {
      $sql = 'UPDATE {movie}';
      $sql .= ' SET ';
      $sql .= 'movie_format_id = %d';
      $sql .= ', uid = %d';
      $sql .= ', imdb_id = \'%s\'';
      $sql .= ', title = \'%s\'';
      $sql .= ', release_date = %d';
      $sql .= ', runtime = %d';
      $sql .= ', active = %d';
      $sql .= ', created = %d';
      $sql .= ', updated = %d';
      $sql .= ' WHERE movie_id = %d';
    }
    db_query($sql
      , $this->movie_format_id
      , $this->uid
      , $this->imdb_id
      , $this->title
      , $this->release_date
      , $this->runtime
      , $this->active
      , $this->created
      , $this->updated
      , $this->movie_id
    );

    if( $this->movie_id == 0 ) {
      $result = db_query('SELECT @@IDENTITY ident');
      $data = db_fetch_object($result);
      $this->movie_id = $data->ident;
      $this->find();
    }

  }
  /*****************************************************
   * End update
   ****************************************************/

  /*****************************************************
   * Begin delete
   ****************************************************/
  // This function will delete the record from table specified by primary key
  public function delete() {

    $sql = 'DELETE FROM {movie} WHERE movie_id = %d';
    db_query($sql, $this->movie_id);
  }
  /*****************************************************
   * End delete
   ****************************************************/

  /*****************************************************
   * Begin get_num_rows
   ****************************************************/
  // This function will return the number of rows/entries in a table
  public function get_num_rows() {

    $sql = 'SELECT COUNT(movie_id) as total_entries FROM {movie}';
    $num_rows = db_fetch_object(db_query($sql));
    return $num_rows->total_entries;
  }
  /*****************************************************
   * End get_num_rows
   ****************************************************/

  /*****************************************************
   * Begin destructor
   ****************************************************/
  public function __destruct() {
    unset(
      $this->movie_id
      , $this->movie_format_id
      , $this->uid
      , $this->imdb_id
      , $this->title
      , $this->release_date
      , $this->runtime
      , $this->active
      , $this->created
      , $this->updated
    );
  }
  /*****************************************************
   * End destructor
   ****************************************************/

}
