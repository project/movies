<?php


  /****************************************************************
   * Class: movie_user_extra
   * Author: Ian Bezanson
   * Date: 2010-08-06
   *
   * The purpose of this class is to extend the base class for
   * user defined functions, properties and methods.
   ***************************************************************/


  include_once('movie_user.php');


  class movie_user_extra extends movie_user {

    /*****************************************************
     * Begin find_by_uid
     ****************************************************/
    // Function finds item from movie_user based on the user id
    public function find_by_uid() {

      $sql = 'SELECT movie_user_id';
      $sql .= ', uid';
      $sql .= ', display_movies';
      $sql .= ', display_wishlist';
      $sql .= ', created';
      $sql .= ', updated';
      $sql .= ' FROM {movie_user}';
      $sql .= ' WHERE uid = %d';
      $result = db_query($sql, $this->uid);

      if( $row = db_fetch_object($result) ) {
        $this->fill_object($row);
        return true;
      }
      else {
        return false;
      }
    }
    /*****************************************************
     * End find_by_uid
     ****************************************************/


    /*****************************************************
     * Begin find_active_users_paginated
     ****************************************************/
    // Finds all records in table or records matching passed where clause
    public function find_active_users_paginated($sorted_header) {

      $sql = 'SELECT mu.movie_user_id';
      $sql .= ', mu.uid';
      $sql .= ', mu.display_movies';
      $sql .= ', mu.display_wishlist';
      $sql .= ', mu.created';
      $sql .= ', mu.updated';
      $sql .= ', u.name';
      $sql .= ', COUNT(m.movie_id) AS num_movies';
      $sql .= ' FROM {movie_user} mu';
      $sql .= ' JOIN {users} u ON mu.uid = u.uid';
      $sql .= ' JOIN {movie} m ON mu.uid = m.uid';
      $sql .= ' AND m.active';
      $sql .= ' WHERE display_movies';
      $sql .= ' GROUP BY mu.uid ';

      $result_count = '
        SELECT
          count(movie_user_id) as this_count
        FROM
        (
          ' . $sql . '
        ) count_table
      ';

      $result = pager_query
      (
        db_rewrite_sql
        ('
          ' . $sql . '
          ' . $sorted_header
          , '{movie_user}'
          , 'movie_user_id'
        ),
        variable_get('movie_user_paginate', 20),
        0,
        $result_count
      );
      return $result;
    }
    /*****************************************************
     * End find_active_users_paginated
     ****************************************************/

  };
?>