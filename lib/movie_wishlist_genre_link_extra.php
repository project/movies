<?php


  /****************************************************************
   * Class: movie_wishlist_genre_link_extra
   * Author: Ian Bezanson
   * Date: 2010-08-06
   *
   * The purpose of this class is to extend the base class for
   * user defined functions, properties and methods.
   ***************************************************************/


  include_once('movie_wishlist_genre_link.php');


  class movie_wishlist_genre_link_extra extends movie_wishlist_genre_link {

    /*****************************************************
     * Begin disable_all_by_movie
     ****************************************************/
    // Function disables all items from movie_wishlist_genre_link based on the
    // movie wishlist id
    public function disable_all_by_movie() {

      $sql = 'UPDATE {movie_wishlist_genre_link}';
      $sql .= ' SET active = 0';
      $sql .= ' , updated = %d';
      $sql .= ' WHERE movie_wishlist_id = %d';

      return db_query(
        $sql,
        mktime(),
        $this->movie_wishlist_id
      );
    }
    /*****************************************************
     * End disable_all_by_movie
     ****************************************************/


    /*****************************************************
     * Begin find_by_details
     ****************************************************/
    // Function finds item from movie_wishlist_genre_link based on the movie
    // wishlist id and movie genre id
    public function find_by_details() {

      $sql = 'SELECT movie_wishlist_genre_link_id';
      $sql .= ', movie_wishlist_id';
      $sql .= ', movie_genre_id';
      $sql .= ', active';
      $sql .= ', created';
      $sql .= ', updated';
      $sql .= ' FROM {movie_wishlist_genre_link}';
      $sql .= ' WHERE movie_wishlist_id = %d';
      $sql .= ' AND movie_genre_id = %d';

      $result = db_query(
        $sql,
        $this->movie_wishlist_id,
        $this->movie_genre_id
      );

      if( $row = db_fetch_object($result) ) {
        $this->fill_object($row);
        return true;
      }
      else {
        return false;
      }
    }
    /*****************************************************
     * End find_by_details
     ****************************************************/

  };
?>