<?php


  /****************************************************************
   * Class: movie_genre_link_extra
   * Author: Ian Bezanson
   * Date: 2010-08-06
   *
   * The purpose of this class is to extend the base class for
   * user defined functions, properties and methods.
   ***************************************************************/


  include_once('movie_genre_link.php');


  class movie_genre_link_extra extends movie_genre_link {

    /*****************************************************
     * Begin disable_all_by_movie
     ****************************************************/
    // Function disables all items from movie_genre_link based on the movie id
    public function disable_all_by_movie() {

      $sql = 'UPDATE {movie_genre_link}';
      $sql .= ' SET active = 0';
      $sql .= ' , updated = %d';
      $sql .= ' WHERE movie_id = %d';

      return db_query(
        $sql,
        mktime(),
        $this->movie_id
      );
    }
    /*****************************************************
     * End disable_all_by_movie
     ****************************************************/


    /*****************************************************
     * Begin find_by_details
     ****************************************************/
    // Function finds item from movie_genre_link based on the movie id and 
    // movie genre id
    public function find_by_details() {

      $sql = 'SELECT movie_genre_link_id';
      $sql .= ', movie_id';
      $sql .= ', movie_genre_id';
      $sql .= ', active';
      $sql .= ', created';
      $sql .= ', updated';
      $sql .= ' FROM {movie_genre_link}';
      $sql .= ' WHERE movie_id = %d';
      $sql .= ' AND movie_genre_id = %d';

      $result = db_query(
        $sql,
        $this->movie_id,
        $this->movie_genre_id
      );

      if( $row = db_fetch_object($result) ) {
        $this->fill_object($row);
        return true;
      }
      else {
        return false;
      }
    }
    /*****************************************************
     * End find_by_details
     ****************************************************/

  };
?>