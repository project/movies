<?php


/**********************************************************************
 * Name: movies.pages.inc
 * Author: Ian Bezanson
 * Date: 2010-08-06
 * 
 * Description: User-facing pages for Movies Drupal Module.
 *********************************************************************/


/**
 * movies_main_page()
 *
 * Main user content page view for Movies
 */
function movies_main_page() {
  $movie_user = new movie_user_extra();

  $output = '';

  $header = array(
    array('data' => t('User'), 'field' => 'name'),
    array('data' => t('# Movies'), 'field' => 'num_movies'),
  );

  $sql = tablesort_sql($header);
  $table_data = $movie_user->find_active_users_paginated($sql);

  if(count($table_data) >= 1) {
    while($row_data = db_fetch_object($table_data)) {
      $single_uid = $row_data->uid;
      $rows[] = array(
        array('data' => l(t($row_data->name), 'movies/view/' . $row_data->uid)),
        array('data' => $row_data->num_movies),
      );
    }
  }
  $output .= '<div id="movie">';

  //If a single row was returned AND the admin has chosen to skip to a single
  //user's listing, go there...
  if(count($rows) == 1 && variable_get('movie_skip_to_listing', 0) == 1) {
    return drupal_goto('movies/view/' . $single_uid);
  }

  //If we have table rows, show said table
  if(count($rows)) {
    $output .= theme('table', $header, $rows);
    $output .= theme('pager', NULL, variable_get('movie_paginate', 50));
  }
  //Otherwise, show a no movies msg
  else {
    $output .= t('There are currently no movies.');
  }

  $output .= '</div>';
  return $output;
}


/**
 * movies_display_page()
 *
 * Show all movies for specified user
 */
function movies_display_page($uid = 0) {

  //Attempt to load the user's movie user record
  $movie_user = new movie_user_extra();
  $movie_user->uid = $uid;
  $movie_user->find_by_uid();
  
  //If the record doesn't exist, or the user has specified that they don't want
  //their movie page to show up, return a page not found.
  if(!$movie_user->movie_user_id || !$movie_user->display_movies) {
    return drupal_not_found();
  }

  //Still here?  Set the user id in the movie object
  $movie = new movie_extra();
  $movie->uid = $uid;

  $output = '';

  //If the user wants their Wishlist page to be displayed, link to it.
  if($movie_user->display_wishlist) {
    $output .= l(t('View My Movie Wishlist Page'), 'movies/wishlist/' . $uid);
  }

  //Set the table header
  $header = array(
    array('data' => t('Movie Title'), 'field' => 'title'),
    array('data' => t('Genre(s)')),
    array('data' => t('Release Date'), 'field' => 'release_date'),
    array('data' => t('Format'), 'field' => 'movie_format'),
    array('data' => t('Date Added'), 'field' => 'created'),
  );

  //Sort SQL based on user clicks and attempt to load all movies for this user
  $sql = tablesort_sql($header);
  $table_data = $movie->find_by_uid_paginated($sql);

  if(count($table_data) >= 1) {
    while($row_data = db_fetch_object($table_data)) {
      //Load all genres attached to this film
      $movie_genre_link = new movie_genre_link_extra();
      $movie_genres = $movie_genre_link->find_many(' movie_id = ' . $row_data->movie_id . ' AND active ');

      $genres = array();

      //If there are active genres tied to this
      if(count($movie_genres)) {
        //Go through each
        foreach($movie_genres as $this_genre) {
          //Load the genre
          $movie_genre = new movie_genre_extra();
          $movie_genre->movie_genre_id = $this_genre->movie_genre_id;
          $movie_genre->find();

          //Add the genre, by name, to the genres array
          $genres[] = $movie_genre->movie_genre;
        }
      }

      $rows[] = array(
        array('data' => ($row_data->imdb_id != '') ? l(t($row_data->title), 'http://www.imdb.com/title/' . $row_data->imdb_id) : $row_data->title),
        array('data' => implode(', ', $genres)),
        array('data' => ($row_data->release_date) ? date("F d, Y", $row_data->release_date) : t('unknown')),
        array('data' => ($row_data->movie_format != '') ? $row_data->movie_format : t('-')),
        array('data' => ($row_data->created) ? format_date($row_data->created, 'small') : t('unknown')),
      );
    }
  }
  $output .= '<div id="movie">';

  //If we have table rows, show said table
  if(count($rows)) {
    $output .= theme('table', $header, $rows);
    $output .= theme('pager', NULL, variable_get('movie_paginate', 50));
  }
  //Otherwise, show a no movies msg
  else {
    $output .= t('There are currently no movies.');
  }

  $output .= '</div>';
  return $output;
}


/**
 * movies_wishlist_display_page()
 *
 * Show all wishlist movies for specified user
 */
function movies_wishlist_display_page($uid = 0) {

  //Attempt to load the user's movie user record
  $movie_user = new movie_user_extra();
  $movie_user->uid = $uid;
  $movie_user->find_by_uid();
  
  //If the record doesn't exist, or the user has specified that they don't want
  //their movie wishlist page to show up, return a page not found.
  if(!$movie_user->movie_user_id || !$movie_user->display_wishlist) {
    return drupal_not_found();
  }

  //Still here?  Set the user id in the movie object
  $movie_wishlist = new movie_wishlist_extra();
  $movie_wishlist->uid = $uid;

  $output = '';

  //If the user wants their Wishlist page to be displayed, link to it.
  if($movie_user->display_movies) {
    $output .= l(t('View My Movie Page'), 'movies/view/' . $uid);
  }

  //Set the table header
  $header = array(
    array('data' => t('Movie Title'), 'field' => 'title'),
    array('data' => t('Genre(s)')),
    array('data' => t('Release Date'), 'field' => 'release_date'),
    array('data' => t('Format'), 'field' => 'movie_format'),
    array('data' => t('Date Added'), 'field' => 'created'),
  );

  //Sort SQL based on user clicks and attempt to load all movies for this user
  $sql = tablesort_sql($header);
  $table_data = $movie_wishlist->find_by_uid_paginated($sql);

  if(count($table_data) >= 1) {
    while($row_data = db_fetch_object($table_data)) {
      //Load all genres attached to this film
      $movie_genre_link = new movie_wishlist_genre_link_extra();
      $movie_genres = $movie_genre_link->find_many(' movie_wishlist_id = ' . $row_data->movie_wishlist_id . ' AND active ');

      $genres = array();

      //If there are active genres tied to this
      if(count($movie_genres)) {
        //Go through each
        foreach($movie_genres as $this_genre) {
          //Load the genre
          $movie_genre = new movie_genre_extra();
          $movie_genre->movie_genre_id = $this_genre->movie_genre_id;
          $movie_genre->find();

          //Add the genre, by name, to the genres array
          $genres[] = $movie_genre->movie_genre;
        }
      }

      $rows[] = array(
        array('data' => ($row_data->imdb_id != '') ? l(t($row_data->title), 'http://www.imdb.com/title/' . $row_data->imdb_id) : $row_data->title),
        array('data' => implode(', ', $genres)),
        array('data' => ($row_data->release_date) ? date("F d, Y", $row_data->release_date) : t('unknown')),
        array('data' => ($row_data->movie_format != '') ? $row_data->movie_format : t('-')),
        array('data' => ($row_data->created) ? format_date($row_data->created, 'small') : t('unknown')),
      );
    }
  }
  $output .= '<div id="movie">';

  //If we have table rows, show said table
  if(count($rows)) {
    $output .= theme('table', $header, $rows);
    $output .= theme('pager', NULL, variable_get('movie_paginate', 50));
  }
  //Otherwise, show a no movies msg
  else {
    $output .= t('There are currently no movies.');
  }

  $output .= '</div>';
  return $output;
}
