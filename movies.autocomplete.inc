<?php


/**********************************************************************
 * Name: movies.autocomplete.inc
 * Author: Ian Bezanson
 * Date: 2010-08-07
 * 
 * Description: Autocompletion pages for Movies Drupal Module.
 *********************************************************************/


/**
 * movies_format_autocomplete()
 *
 * Return first ten results from movie format table, based on what user is
 * typing.
 */
function movies_format_autocomplete($movie_format = '') {
  $matches = array();

  $result = db_query_range("SELECT movie_format FROM {movie_format} where LOWER(movie_format) LIKE LOWER('%s%%')", $movie_format, 0, 10);

  while ($row = db_fetch_object($result)) {
    $matches[$row->movie_format] = check_plain($row->movie_format);
  }

  print drupal_to_js($matches);
  exit();
}
