<?php


/**********************************************************************
 * Name: movies.admin.inc
 * Author: Ian Bezanson
 * Date: 2010-08-06
 * 
 * Description: Administrative functions for Movies Drupal Module.
 *********************************************************************/


/**
 * Movies Administrative Settings Page
 */
function movies_admin_settings() {
  $items = array();

  $items['movie_paginate'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Movies Listings Per Page'),
    '#default_value' => variable_get('movie_paginate', 50),
    '#size'          => 5,
    '#maxlength'     => 5,
    '#description'   => t('How many entries would you like to display per Movies page?'),
  );

  $items['movie_skip_to_listing'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Skip To Movie List'),
    '#default_value' => variable_get('movie_skip_to_listing', 0),
    '#description'   => t('This really is only applied (and applicable) to a single-user movie list.  Rather than presenting a list of one Movie list, would you prefer to simply send the user directly to your movie listings page?'),
  );

  return system_settings_form($items);
}


/**
 * movies_admin_main()
 *
 * This is the main page for all CRUD (Create, Retrieve, Update and Delete)
 * tasks associated with DB tables/classes for this module.
 */
function movies_admin_main() {
  $movie = new movie_extra();
  $movie_wishlist = new movie_wishlist_extra();
  $movie_format = new movie_format_extra();
  $movie_genre = new movie_genre_extra();

  $output = '';

  $header = array(
    array('data' => t('<b>Table Name</b>')),
    array('data' => t('<b>No Entries</b>')),
  );

  $rows[] = array(
    array('data' => t('Movie Items')),
    array('data' => $movie->get_num_rows()),
  );

  $rows[] = array(
    array('data' => t('Movie Wishlist Items')),
    array('data' => $movie_wishlist->get_num_rows()),
  );

  $rows[] = array(
    array('data' => l(t('Movie Formats'), 'admin/content/movies/movie_format')),
    array('data' => $movie_format->get_num_rows()),
  );

  $rows[] = array(
    array('data' => l(t('Movie Genres'), 'admin/content/movies/movie_genre')),
    array('data' => $movie_genre->get_num_rows()),
  );

  $output .= theme('table', $header, $rows);

  return $output;
}


/**
 * movies_admin_add_movie_format()
 *
 * Form hook
 *
 * This function will present the user with a form to add a new entry
 * to the Movie Format table in the database. 
 */
function movies_admin_add_movie_format(&$form_state) {
  $form['movie_format'] = array(
    '#title' => t('Movie Format'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#cols' => 50,
    '#rows' => 5,
  );

  $form['active'] = array(
    '#title' => t('Active'),
    '#type' => 'checkbox',
  );

  $form['movie_format_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}


/**
 * movies_admin_add_movie_format_submit()
 *
 * hook_submit
 *
 * This function will go through all fields in movie_format, setting
 * each to the user-supplied values.  The primary key is set to 0
 * so that the class is aware that this is a new entry.
 */
function movies_admin_add_movie_format_submit($form, &$form_state) {
  $movie_format = new movie_format_extra();

  $movie_format->movie_format = $form_state['values']['movie_format'];
  $movie_format->description = $form_state['values']['description'];
  $movie_format->active = $form_state['values']['active'];
  $movie_format->created = mktime();
  $movie_format->updated = 0;
  $movie_format->update();

  return drupal_goto('admin/content/movies/movie_format');
}


/**
 * movies_admin_edit_movie_format()
 *
 * Form hook
 *
 * This function will find the id from the movie_format class and display
 * a form with existing data, allowing the user to change all but the 
 * primary key.
 */
function movies_admin_edit_movie_format(&$form_state, $id = 0) {
  $movie_format = new movie_format_extra();
  $movie_format->movie_format_id = $id;
  $movie_format->find();

  $form['movie_format_id_hidden'] = array(
    '#type' => 'hidden',
    '#value' => $movie_format->movie_format_id,
  );

  $form['movie_format_id'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => $movie_format->movie_format_id,
    '#title' => t('Id'),
    '#disabled' => TRUE,
  );

  $form['movie_format'] = array(
    '#title' => t('Movie Format'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => $movie_format->movie_format,
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#cols' => 50,
    '#rows' => 5,
    '#default_value' => $movie_format->description,
  );

  $form['active'] = array(
    '#title' => t('Active'),
    '#type' => 'checkbox',
    '#default_value' => $movie_format->active,
  );

  $form['created'] = array(
    '#title' => t('Created'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 50,
    '#default_value' => ($movie_format->created) ? format_date($movie_format->created) : t('never'),
    '#disabled' => TRUE,
  );

  $form['updated'] = array(
    '#title' => t('Updated'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 50,
    '#default_value' => ($movie_format->updated) ? format_date($movie_format->updated) : t('never'),
    '#disabled' => TRUE,
  );

  $form['movie_format_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}


/**
 * movies_admin_edit_movie_format_submit()
 *
 * hook_submit
 *
 * This function will find the id from the movie_format class and update
 * the corresponding entry with supplied data.  Then the user will be 
 * sent back to the view function.
 */
function movies_admin_edit_movie_format_submit($form, &$form_state) {
  $movie_format = new movie_format_extra();
  $movie_format->movie_format_id = $form_state['values']['movie_format_id_hidden'];
  $movie_format->find();

  $movie_format->movie_format = $form_state['values']['movie_format'];
  $movie_format->description = $form_state['values']['description'];
  $movie_format->active = $form_state['values']['active'];
  $movie_format->updated = mktime();
  $movie_format->update();

  return drupal_goto('admin/content/movies/movie_format');
}


/**
 * movies_admin_delete_movie_format()
 *
 * Form hook
 *
 * This function will find the id from the movie_format class and display
 * an uneditable form with existing data, allowing the user to confirm 
 * that they wish to delete this entry.
 */
function movies_admin_delete_movie_format(&$form_state, $id = 0) {
  $movie_format = new movie_format_extra();
  $movie_format->movie_format_id = $id;
  $movie_format->find();

  $form['movie_format_id_hidden'] = array(
    '#type' => 'hidden',
    '#value' => $movie_format->movie_format_id,
  );

  $form['movie_format_id'] = array(
    '#title' => t('Id'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => $movie_format->movie_format_id,
    '#disabled' => TRUE,
  );

  $form['movie_format'] = array(
    '#title' => t('Movie Format'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => $movie_format->movie_format,
    '#disabled' => TRUE,
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#cols' => 50,
    '#rows' => 5,
    '#default_value' => $movie_format->description,
    '#disabled' => TRUE,
  );

  $form['active'] = array(
    '#title' => t('Active'),
    '#type' => 'checkbox',
    '#default_value' => $movie_format->active,
    '#disabled' => TRUE,
  );

  $form['created'] = array(
    '#title' => t('Created'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 50,
    '#default_value' => ($movie_format->created) ? format_date($movie_format->created) : t('never'),
    '#disabled' => TRUE,
  );

  $form['updated'] = array(
    '#title' => t('Updated'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 50,
    '#default_value' => ($movie_format->updated) ? format_date($movie_format->updated) : t('never'),
    '#disabled' => TRUE,
  );

  $form['movie_format_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm Delete'),
  );

  $form['movie_format_cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}


/**
 * movies_admin_delete_movie_format_submit()
 *
 * hook_submit
 *
 * This function will find the id from the movie_format class and delete
 * the corresponding entry.  Then the user will be sent back to the view
 * function.
 */
function movies_admin_delete_movie_format_submit($form, &$form_state) {
  $movie_format = new movie_format_extra();

  if($form_state['values']['op'] == t('Cancel')) {
    return drupal_goto('admin/content/movies/movie_format');
  }

  $movie_format->movie_format_id = $form_state['values']['movie_format_id_hidden'];
  $movie_format->find();
  $movie_format->delete();

  return drupal_goto('admin/content/movies/movie_format');
}


/**
 * movies_admin_view_movie_format()
 *
 * Main table view of all entries in Movie Format.
 */
function movies_admin_view_movie_format() {
  $movie_format = new movie_format_extra();
  $output = '';

  $header = array(
    array('data' => t('Id'), 'field' => 'movie_format_id'),
    array('data' => t('Movie Format'), 'field' => 'movie_format'),
    array('data' => t('Description'), 'field' => 'description'),
    array('data' => t('Active'), 'field' => 'active'),
    array('data' => t('Created'), 'field' => 'created'),
    array('data' => t('Updated'), 'field' => 'updated'),
    array('data' => t('Options')),
  );
  $sql = tablesort_sql($header);
  $table_data = $movie_format->find_many_paginated($sql);
  if(count($table_data) >= 1) {
    while($row_data = db_fetch_object($table_data)){
      $rows[] = array(
        array('data' => $row_data->movie_format_id),
        array('data' => $row_data->movie_format),
        array('data' => $row_data->description),
        array('data' => ($row_data->active) ? t('yes') : t('no')),
        array('data' => ($row_data->created) ? format_date($row_data->created) : t('never')),
        array('data' => ($row_data->updated) ? format_date($row_data->updated) : t('never')),
        array('data' => l(t('Edit'), 'admin/content/movies/movie_format/edit/' . $row_data->movie_format_id) . ' | ' . l(t('Delete'), 'admin/content/movies/movie_format/delete/' . $row_data->movie_format_id)),
      );
    }
  }
  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, variable_get('movie_paginate', 50));
  return $output;
}


/**
 * movies_admin_add_movie_genre()
 *
 * Form hook
 *
 * This function will present the user with a form to add a new entry
 * to the Movie Genre table in the database. 
 */
function movies_admin_add_movie_genre(&$form_state) {
  $form['movie_genre'] = array(
    '#title' => t('Movie Genre'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#cols' => 50,
    '#rows' => 5,
  );

  $form['active'] = array(
    '#title' => t('Active'),
    '#type' => 'checkbox',
  );

  $form['movie_genre_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}


/**
 * movies_admin_add_movie_genre_submit()
 *
 * hook_submit
 *
 * This function will go through all fields in movie_genre, setting
 * each to the user-supplied values.  The primary key is set to 0
 * so that the class is aware that this is a new entry.
 */
function movies_admin_add_movie_genre_submit($form, &$form_state) {
  $movie_genre = new movie_genre_extra();

  $movie_genre->movie_genre = $form_state['values']['movie_genre'];
  $movie_genre->description = $form_state['values']['description'];
  $movie_genre->active = $form_state['values']['active'];
  $movie_genre->created = mktime();
  $movie_genre->updated = 0;
  $movie_genre->update();

  return drupal_goto('admin/content/movies/movie_genre');
}


/**
 * movies_admin_edit_movie_genre()
 *
 * Form hook
 *
 * This function will find the id from the movie_genre class and display
 * a form with existing data, allowing the user to change all but the 
 * primary key.
 */
function movies_admin_edit_movie_genre(&$form_state, $id = 0) {
  $movie_genre = new movie_genre_extra();
  $movie_genre->movie_genre_id = $id;
  $movie_genre->find();

  $form['movie_genre_id_hidden'] = array(
    '#type' => 'hidden',
    '#value' => $movie_genre->movie_genre_id,
  );

  $form['movie_genre_id'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => $movie_genre->movie_genre_id,
    '#title' => t('Id'),
    '#disabled' => TRUE,
  );

  $form['movie_genre'] = array(
    '#title' => t('Movie Genre'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => $movie_genre->movie_genre,
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#cols' => 50,
    '#rows' => 5,
    '#default_value' => $movie_genre->description,
  );

  $form['active'] = array(
    '#title' => t('Active'),
    '#type' => 'checkbox',
    '#default_value' => $movie_genre->active,
  );

  $form['created'] = array(
    '#title' => t('Created'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 50,
    '#default_value' => ($movie_genre->created) ? format_date($movie_genre->created) : t('never'),
    '#disabled' => TRUE,
  );

  $form['updated'] = array(
    '#title' => t('Updated'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 50,
    '#default_value' => ($movie_genre->updated) ? format_date($movie_genre->updated) : t('never'),
    '#disabled' => TRUE,
  );

  $form['movie_genre_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}


/**
 * movies_admin_edit_movie_genre_submit()
 *
 * hook_submit
 *
 * This function will find the id from the movie_genre class and update
 * the corresponding entry with supplied data.  Then the user will be 
 * sent back to the view function.
 */
function movies_admin_edit_movie_genre_submit($form, &$form_state) {
  $movie_genre = new movie_genre_extra();
  $movie_genre->movie_genre_id = $form_state['values']['movie_genre_id_hidden'];
  $movie_genre->find();

  $movie_genre->movie_genre = $form_state['values']['movie_genre'];
  $movie_genre->description = $form_state['values']['description'];
  $movie_genre->active = $form_state['values']['active'];
  $movie_genre->updated = mktime();
  $movie_genre->update();

  return drupal_goto('admin/content/movies/movie_genre');
}


/**
 * movies_admin_delete_movie_genre()
 *
 * Form hook
 *
 * This function will find the id from the movie_genre class and display
 * an uneditable form with existing data, allowing the user to confirm 
 * that they wish to delete this entry.
 */
function movies_admin_delete_movie_genre(&$form_state, $id = 0) {
  $movie_genre = new movie_genre_extra();
  $movie_genre->movie_genre_id = $id;
  $movie_genre->find();

  $form['movie_genre_id_hidden'] = array(
    '#type' => 'hidden',
    '#value' => $movie_genre->movie_genre_id,
  );

  $form['movie_genre_id'] = array(
    '#title' => t('Id'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => $movie_genre->movie_genre_id,
    '#disabled' => TRUE,
  );

  $form['movie_genre'] = array(
    '#title' => t('Movie Genre'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => $movie_genre->movie_genre,
    '#disabled' => TRUE,
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#cols' => 50,
    '#rows' => 5,
    '#default_value' => $movie_genre->description,
    '#disabled' => TRUE,
  );

  $form['active'] = array(
    '#title' => t('Active'),
    '#type' => 'checkbox',
    '#default_value' => $movie_genre->active,
    '#disabled' => TRUE,
  );

  $form['created'] = array(
    '#title' => t('Created'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 50,
    '#default_value' => ($movie_genre->created) ? format_date($movie_genre->created) : t('never'),
    '#disabled' => TRUE,
  );

  $form['updated'] = array(
    '#title' => t('Updated'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 50,
    '#default_value' => ($movie_genre->updated) ? format_date($movie_genre->updated) : t('never'),
    '#disabled' => TRUE,
  );

  $form['movie_genre_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm Delete'),
  );

  $form['movie_genre_cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}


/**
 * movies_admin_delete_movie_genre_submit()
 *
 * hook_submit
 *
 * This function will find the id from the movie_genre class and delete
 * the corresponding entry.  Then the user will be sent back to the view
 * function.
 */
function movies_admin_delete_movie_genre_submit($form, &$form_state) {
  $movie_genre = new movie_genre_extra();

  if($form_state['values']['op'] == t('Cancel')) {
    return drupal_goto('admin/content/movies/movie_genre');
  }

  $movie_genre->movie_genre_id = $form_state['values']['movie_genre_id_hidden'];
  $movie_genre->find();
  $movie_genre->delete();

  return drupal_goto('admin/content/movies/movie_genre');
}


/**
 * movies_admin_view_movie_genre()
 *
 * Main table view of all entries in Movie Genre.
 */
function movies_admin_view_movie_genre() {
  $movie_genre = new movie_genre_extra();
  $output = '';

  $header = array(
    array('data' => t('Id'), 'field' => 'movie_genre_id'),
    array('data' => t('Movie Genre'), 'field' => 'movie_genre'),
    array('data' => t('Description'), 'field' => 'description'),
    array('data' => t('Active'), 'field' => 'active'),
    array('data' => t('Created'), 'field' => 'created'),
    array('data' => t('Updated'), 'field' => 'updated'),
    array('data' => t('Options')),
  );
  $sql = tablesort_sql($header);
  $table_data = $movie_genre->find_many_paginated($sql);
  if(count($table_data) >= 1) {
    while($row_data = db_fetch_object($table_data)){
      $rows[] = array(
        array('data' => $row_data->movie_genre_id),
        array('data' => $row_data->movie_genre),
        array('data' => $row_data->description),
        array('data' => ($row_data->active) ? t('yes') : t('no')),
        array('data' => ($row_data->created) ? format_date($row_data->created) : t('never')),
        array('data' => ($row_data->updated) ? format_date($row_data->updated) : t('never')),
        array('data' => l(t('Edit'), 'admin/content/movies/movie_genre/edit/' . $row_data->movie_genre_id) . ' | ' . l(t('Delete'), 'admin/content/movies/movie_genre/delete/' . $row_data->movie_genre_id)),
      );
    }
  }
  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, variable_get('movie_paginate', 50));
  return $output;
}
