<?php


/**********************************************************************
 * Name: movies.user.inc
 * Author: Ian Bezanson
 * Date: 2010-08-07
 * 
 * Description: Pages from My Account section, specific to Movies
 *              Drupal Module.
 *********************************************************************/


/**
 * movies_user_page()
 *
 * Main user content page view for Movies
 */
function movies_user_page($account) {
  global $user;

  if(!user_edit_access($account)) {
    return drupal_access_denied();
  }

  //Set the page title with what data is available.
  $title = isset($account->title) ? $account->title : $account->name;
  drupal_set_title(check_plain($title));

  $movie = new movie_extra();
  $output = '';
  $output .= l(t('Add New Movie'), 'user/' . $account->uid . '/movies/movies/add_movie');

  $header = array(
    array('data' => t('Id'), 'field' => 'movie_id'),
    array('data' => t('Movie Title'), 'field' => 'title'),
    array('data' => t('Release Date'), 'field' => 'release_date'),
    array('data' => t('IMDB'), 'field' => 'imdb_id'),
    array('data' => t('Active'), 'field' => 'active'),
  );
  $sql = tablesort_sql($header);
  $table_data = $movie->find_many_paginated($sql, ' uid = ' . $account->uid);
  if(count($table_data) >= 1) {
    while($row_data = db_fetch_object($table_data)) {
      $rows[] = array(
        array('data' => l(t($row_data->movie_id), 'user/' . $account->uid . '/movies/movies/add_movie/' . $row_data->movie_id)),
        array('data' => $row_data->title),
        array('data' => ($row_data->release_date) ? date("F d, Y", $row_data->release_date) : t('unknown')),
        array('data' => ($row_data->imdb_id != '') ? l(t($row_data->imdb_id), 'http://www.imdb.com/title/' . $row_data->imdb_id) : ''),
        array('data' => ($row_data->active) ? t('yes') : t('no')),
      );
    }
  }
  $output .= '<div id="movie">';

  //If we have table rows, show said table
  if(count($rows)) {
    $output .= theme('table', $header, $rows);
    $output .= theme('pager', NULL, variable_get('movie_paginate', 50));
  }
  //Otherwise, show a no movies msg
  else {
    $output .= t('There are currently no movies.');
  }

  $output .= '</div>';
  return $output;
}


/**
 * movies_wishlist_user_page()
 *
 * Main user content page view for Movie Wishlists
 */
function movies_wishlist_user_page($account) {
  global $user;

  if(!user_edit_access($account)) {
    return drupal_access_denied();
  }

  //Set the page title with what data is available.
  $title = isset($account->title) ? $account->title : $account->name;
  drupal_set_title(check_plain($title));

  $movie_wishlist = new movie_wishlist_extra();
  $output = '';
  $output .= l(t('Add New Movie'), 'user/' . $account->uid . '/movies/wishlist/add_movie');

  $header = array(
    array('data' => t('Id'), 'field' => 'movie_wishlist_id'),
    array('data' => t('Movie Title'), 'field' => 'title'),
    array('data' => t('Release Date'), 'field' => 'release_date'),
    array('data' => t('IMDB'), 'field' => 'imdb_id'),
    array('data' => t('Active'), 'field' => 'active'),
    array('data' => t('Got It!'), 'field' => 'added_to_movies'),
  );
  $sql = tablesort_sql($header);
  $table_data = $movie_wishlist->find_many_paginated($sql, ' uid = ' . $account->uid . ' AND !added_to_movies ');
  if(count($table_data) >= 1) {
    while($row_data = db_fetch_object($table_data)) {
      $rows[] = array(
        array('data' => l(t($row_data->movie_wishlist_id), 'user/' . $account->uid . '/movies/wishlist/add_movie/' . $row_data->movie_wishlist_id)),
        array('data' => $row_data->title),
        array('data' => ($row_data->release_date) ? date("F d, Y", $row_data->release_date) : t('unknown')),
        array('data' => ($row_data->imdb_id != '') ? l(t($row_data->imdb_id), 'http://www.imdb.com/title/' . $row_data->imdb_id) : ''),
        array('data' => ($row_data->active) ? t('yes') : t('no')),
        array('data' => l(t('Add To Movies'), 'user/' . $account->uid . '/movies/wishlist/add_to_movies/' . $row_data->movie_wishlist_id)),
      );
    }
  }
  $output .= '<div id="movie_wishlist">';

  //If we have table rows, show said table
  if(count($rows)) {
    $output .= theme('table', $header, $rows);
    $output .= theme('pager', NULL, variable_get('movie_paginate', 50));
  }
  //Otherwise, show a no movies msg
  else {
    $output .= t('There are currently no movies in your wishlist.');
  }

  $output .= '</div>';
  return $output;
}


/**
 * movies_user_view_conflicts()
 *
 * Present a list of movies, for this user, which exist in both their movies and
 * movie_wishlist.
 */
function movies_user_view_conflicts($account) {
  global $user;

  if(!user_edit_access($account)) {
    return drupal_access_denied();
  }

  //Set the page title with what data is available.
  $title = isset($account->title) ? $account->title : $account->name;
  drupal_set_title(check_plain($title));

  $movie = new movie_extra();
  $movie->uid = $account->uid;
  $output = '';

  $header = array(
    array('data' => t('Movie Title'), 'field' => 'title'),
    array('data' => t('Release Date'), 'field' => 'release_date'),
    array('data' => t('IMDB'), 'field' => 'imdb_id'),
    array('data' => t('Active'), 'field' => 'active'),
    array('data' => t('Fix Conflict?')),
  );
  $sql = tablesort_sql($header);
  $table_data = $movie->find_conflicts_paginated($sql);
  if(count($table_data) >= 1) {
    while($row_data = db_fetch_object($table_data)) {
      $rows[] = array(
        array('data' => $row_data->title),
        array('data' => ($row_data->release_date) ? date("F d, Y", $row_data->release_date) : t('unknown')),
        array('data' => ($row_data->imdb_id != '') ? l(t($row_data->imdb_id), 'http://www.imdb.com/title/' . $row_data->imdb_id) : ''),
        array('data' => ($row_data->active) ? t('yes') : t('no')),
        array('data' => l(t('Fix This!'), 'user/' . $account->uid . '/movies/conflicts/fix_conflict/' . $row_data->movie_wishlist_id)),
      );
    }
  }
  $output .= '<div id="movie_conflicts">';

  //If we have table rows, show said table
  if(count($rows)) {
    $output .= theme('table', $header, $rows);
    $output .= theme('pager', NULL, variable_get('movie_paginate', 50));
  }
  //Otherwise, show a no movies msg
  else {
    $output .= t('There are no conflicts between the Movie list and the Movie Wishlist!');
  }

  $output .= '</div>';
  return $output;
}


/**
 * movies_user_add()
 *
 * Main form to add movie items
 */
function movies_user_add(&$form_state, $account, $movie_id = 0) {
  //Make the system think that we're on the main wishlist page, to ensure that
  //page tabs remain in place.
  menu_set_active_item('user/' . $account->uid . '/movies/movies');

  //Load user object for this user
  global $user;

  //If this user shouldn't be here, show them access denied
  if(!user_edit_access($account)) {
    return drupal_access_denied();
  }

  //Set the page title with what data is available.
  $title = isset($account->title) ? $account->title : $account->name;
  drupal_set_title(check_plain($title));

  $movie = new movie_extra();
  $movie->movie_id = $movie_id;
  $movie->find();

  //Assume this user cannot edit the page
  $show_edit = FALSE;

  //If the movie exists AND this is either the owning user, or one who has 
  //permissions to "Administer Users", they can be here.
  if(((($movie->uid == $user->uid) || user_access('administer users')) && $user->uid > 0) && $movie->movie_id) {
    $show_edit = TRUE;
  }
  //Otherwise, if it's a request for a new film, they're fine.
  else {
    if(!$movie->movie_id) {
      $show_edit = TRUE;
    }
  }

  //If user is not allowed to be here, display a message and send them back to
  //the account edit page.
  if($show_edit == FALSE) {
    drupal_set_message('You do not have access to edit this film');
    return drupal_goto('user/' . $account->uid . '/movies/movies');
  }

  $form['movie_id_hidden'] = array(
    '#type' => 'hidden',
    '#value' => $movie->movie_id,
  );

  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $account->uid,
  );

  $form['title'] = array(
    '#title' => t('Movie Title'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => $movie->title,
    '#required' => TRUE,
  );

  //Attempt to load the existing movie format
  $movie_format = new movie_format_extra();
  $movie_format->movie_format_id = $movie->movie_format_id;
  $movie_format->find();

  $form['movie_format'] = array(
    '#title' => t('Movie Format'),
    '#description' => t('Enter storage format of film (e.g. Bluray, DVD, etc.)'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => $movie_format->movie_format,
    '#autocomplete_path' => 'movies/autocomplete/format',
  );

  //Attempt to load existing film genres linked to this movie
  $movie_genre_link = new movie_genre_link_extra();
  $movie_genres = $movie_genre_link->find_many(' movie_id = ' . $movie_id . ' AND active ');

  $genres = array();

  //If there are active genres tied to this
  if(count($movie_genres)) {
    //Go through each
    foreach($movie_genres as $this_genre) {
      //Load the genre
      $movie_genre = new movie_genre_extra();
      $movie_genre->movie_genre_id = $this_genre->movie_genre_id;
      $movie_genre->find();

      //Add the genre, by name, to the genres array
      $genres[] = $movie_genre->movie_genre;
    }
  }

  $form['movie_genres'] = array(
    '#title' => t('Movie Genre(s)'),
    '#description' => t('Enter a comma-separated list of film genres'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => implode(', ', $genres),
  );

  $form['imdb_id'] = array(
    '#title' => t('IMDB #'),
    '#description' => t('IMDB reference number for this film, if applicable (e.g. tt1375666)'),
    '#type' => 'textfield',
    '#size' => 20,
    '#maxlength' => 20,
    '#default_value' => $movie->imdb_id,
  );

  //We're not requiring the date_api module, but will enhance our view if it's
  //here.
  if(module_exists('date_api')) {
    $form['release_date'] = array(
      '#title' => t('Movie Release Date'),
      '#description' => t('Day the film was released'),
      '#type' => 'date_popup',
      '#date_format' => 'F d, Y',
      '#date_year_range' => '1900:+3',
      '#default_value' => ($movie->release_date) ? date("Y-m-d", $movie->release_date) : t(''),
    );
  }
  else {
    $form['release_date'] = array(
      '#title' => t('Movie Release Date'),
      '#description' => t('Day the film was released (Format: August 07, 2010)'),
      '#type' => 'textfield',
      '#size' => 50,
      '#maxlength' => 255,
      '#default_value' => ($movie->release_date) ? date("F d, Y", $movie->release_date) : t(''),
    );
  }

  $form['runtime'] = array(
    '#title' => t('Movie Runtime'),
    '#description' => t('Runtime, in minutes, of film (e.g. 122)'),
    '#type' => 'textfield',
    '#size' => 11,
    '#maxlength' => 11,
    '#default_value' => $movie->runtime,
  );

  $form['active'] = array(
    '#type' => 'hidden',
    '#default_value' => $movie->active,
  );

  //If this is an existing entry, elaborate a little.
  if($movie_id) {
    $form['active']['#type'] = 'checkbox';
    $form['active']['#title'] = t('Active');

    $form['created'] = array(
      '#title' => t('Created'),
      '#type' => 'textfield',
      '#size' => 50,
      '#maxlength' => 50,
      '#default_value' => ($movie->created) ? format_date($movie->created) : t('never'),
      '#disabled' => TRUE,
    );

    $form['updated'] = array(
      '#title' => t('Updated'),
      '#type' => 'textfield',
      '#size' => 50,
      '#maxlength' => 50,
      '#default_value' => ($movie->updated) ? format_date($movie->updated) : t('never'),
      '#disabled' => TRUE,
    );
  }

  $form['movie_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}


/**
 * movies_user_add_validate()
 *
 * hook_validate
 *
 * Validate movie record before attempting to save
 */
function movies_user_add_validate($form, &$form_state) {
  //If the release date was provided, ensure it's in a proper format
  if($form_state['values']['release_date'] != '') {
    if(!strtotime($form_state['values']['release_date'])) {
      form_set_error('release_date', t('Invalid release date, please use format: August 07, 2010'));
    }
  }

  //If Runtime was provided, ensure it's numeric
  if(!is_numeric($form_state['values']['runtime'])) {
    form_set_error('runtime', t('Invalid runtime!  Only numbers can be used here, representing runtime in minutes of the film (e.g. 123)'));
  }

  //If the IMDB # was provided, ensure it follows loose standards of what we 
  //know of these ids.
  if($form_state['values']['imdb_id'] != '') {
    if(!preg_match('/^tt(\d+)$/', $form_state['values']['imdb_id'])) {
      form_set_error('imdb_id', t('Invalid IMDB #, please use format: tt1375666'));
    }
  }
}


/**
 * movies_user_add_submit()
 *
 * hook_submit
 *
 * Add or update the movie record, tieing into play the format and genre(s)
 * specified.
 */
function movies_user_add_submit($form, &$form_state) {
  //Attempt to load the existing movie record
  $movie = new movie_extra();
  $movie->movie_id = $form_state['values']['movie_id_hidden'];
  $movie->find();

  //Attempt to load the specified format
  $movie_format = new movie_format_extra();
  $movie_format->movie_format = $form_state['values']['movie_format'];

  //If the format has a value...
  if($form_state['values']['movie_format'] != '') {
    $movie_format->find_by_format();

    //If it doesn't exist, create it
    if(!$movie_format->movie_format_id) {
      $movie_format->created = mktime();
      $movie_format->update();
    }
  }

  //Build the rest of the movie record, based on details filled in by user.
  $movie->movie_format_id = $movie_format->movie_format_id;
  $movie->uid = $form_state['values']['uid'];
  $movie->imdb_id = $form_state['values']['imdb_id'];
  $movie->title = $form_state['values']['title'];
  $movie->release_date = strtotime($form_state['values']['release_date']);
  $movie->runtime = $form_state['values']['runtime'];
  $movie->active = $form_state['values']['active'];

  //If movie already exists
  if($movie->movie_id) {
    //Set update time to now
    $movie->updated = mktime();
  }
  //Otherwise...
  else {
    //Set created time to now
    $movie->created = mktime();
  }

  //Update movie record
  $movie->update();

  //Disable any genre listings for this film
  $movie_genre_link = new movie_genre_link_extra();
  $movie_genre_link->movie_id = $movie->movie_id;
  $movie_genre_link->disable_all_by_movie();

  //If movie genres were provided
  if($form_state['values']['movie_genres']) {
    //Break them apart into single listings
    $movie_genres = split(',', $form_state['values']['movie_genres']);

    //If there are movie genres
    if(count($movie_genres)) {
      //Go through each
      foreach($movie_genres as $this_genre) {
        //Get rid of any prefix spacing
        $this_genre = preg_replace('/^(\s+)/', '', $this_genre);

        //Get rid of any suffix spacing
        $this_genre = preg_replace('/(\s+)$/', '', $this_genre);

        //See if we can find this genre
        $movie_genre = new movie_genre_extra();
        $movie_genre->movie_genre = $this_genre;
        $movie_genre->find_by_genre();

        //It doesn't exist.  Create it
        if(!$movie_genre->movie_genre_id) {
          $movie_genre->created = mktime();
          $movie_genre->update();
        }

        //Check to see if an existing movie-genre link exists
        $movie_genre_link = new movie_genre_link_extra();
        $movie_genre_link->movie_id = $movie->movie_id;
        $movie_genre_link->movie_genre_id = $movie_genre->movie_genre_id;
        $movie_genre_link->find_by_details();
        
        //One exists
        if($movie_genre_link->movie_genre_link_id) {
          //Make it active and set the updated time
          $movie_genre_link->active = 1;
          $movie_genre_link->updated = mktime();
        }
        //None exists
        else {
          //Make it active and set the created time
          $movie_genre_link->active = 1;
          $movie_genre_link->created = mktime();
        }

        //Update the record
        $movie_genre_link->update();
      }
    }
  }

  //Send user back to their movie listings page
  return drupal_goto('user/' . $form_state['values']['uid'] . '/movies');
}


/**
 * movies_wishlist_user_add()
 *
 * Main form to add wishlist items
 */
function movies_wishlist_user_add(&$form_state, $account, $movie_wishlist_id = 0) {
  //Make the system think that we're on the main wishlist page, to ensure that
  //page tabs remain in place.
  menu_set_active_item('user/' . $account->uid . '/movies/wishlist');

  //Load user object for this user
  global $user;

  //If this user shouldn't be here, show them access denied
  if(!user_edit_access($account)) {
    return drupal_access_denied();
  }

  //Set the page title with what data is available.
  $title = isset($account->title) ? $account->title : $account->name;
  drupal_set_title(check_plain($title));

  $movie_wishlist = new movie_wishlist_extra();
  $movie_wishlist->movie_wishlist_id = $movie_wishlist_id;
  $movie_wishlist->find();

  //Assume this user cannot edit the page
  $show_edit = FALSE;

  //If the movie exists AND this is either the owning user, or one who has 
  //permissions to "Administer Users", they can be here.
  if(((($movie_wishlist->uid == $user->uid) || user_access('administer users')) && $user->uid > 0) && $movie_wishlist->movie_wishlist_id) {
    $show_edit = TRUE;
  }
  //Otherwise, if it's a request for a new film, they're fine.
  else {
    if(!$movie_wishlist->movie_wishlist_id) {
      $show_edit = TRUE;
    }
  }

  //If user is not allowed to be here, display a message and send them back to
  //the account edit page.
  if($show_edit == FALSE) {
    drupal_set_message('You do not have access to edit this film');
    return drupal_goto('user/' . $account->uid . '/movies/wishlist');
  }

  $form['movie_wishlist_id_hidden'] = array(
    '#type' => 'hidden',
    '#value' => $movie_wishlist->movie_wishlist_id,
  );

  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $account->uid,
  );

  $form['title'] = array(
    '#title' => t('Movie Title'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => $movie_wishlist->title,
    '#required' => TRUE,
  );

  //Attempt to load the existing movie format
  $movie_format = new movie_format_extra();
  $movie_format->movie_format_id = $movie_wishlist->movie_format_id;
  $movie_format->find();

  $form['movie_format'] = array(
    '#title' => t('Movie Format'),
    '#description' => t('Enter storage format of film (e.g. Bluray, DVD, etc.)'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => $movie_format->movie_format,
    '#autocomplete_path' => 'movies/autocomplete/format',
  );

  //Attempt to load existing film genres linked to this movie
  $movie_wishlist_genre_link = new movie_wishlist_genre_link_extra();
  $movie_wishlist_genres = $movie_wishlist_genre_link->find_many(' movie_wishlist_id = ' . $movie_wishlist_id . ' AND active ');

  $genres = array();

  //If there are active genres tied to this
  if(count($movie_wishlist_genres)) {
    //Go through each
    foreach($movie_wishlist_genres as $this_genre) {
      //Load the genre
      $movie_genre = new movie_genre_extra();
      $movie_genre->movie_genre_id = $this_genre->movie_genre_id;
      $movie_genre->find();

      //Add the genre, by name, to the genres array
      $genres[] = $movie_genre->movie_genre;
    }
  }

  $form['movie_genres'] = array(
    '#title' => t('Movie Genre(s)'),
    '#description' => t('Enter a comma-separated list of film genres'),
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => implode(', ', $genres),
  );

  $form['imdb_id'] = array(
    '#title' => t('IMDB #'),
    '#description' => t('IMDB reference number for this film, if applicable (e.g. tt1375666)'),
    '#type' => 'textfield',
    '#size' => 20,
    '#maxlength' => 20,
    '#default_value' => $movie_wishlist->imdb_id,
  );

  //We're not requiring the date_api module, but will enhance our view if it's
  //here.
  if(module_exists('date_api')) {
    $form['release_date'] = array(
      '#title' => t('Movie Release Date'),
      '#description' => t('Day the film was released'),
      '#type' => 'date_popup',
      '#date_format' => 'F d, Y',
      '#date_year_range' => '1900:+3',
      '#default_value' => ($movie_wishlist->release_date) ? date("Y-m-d", $movie_wishlist->release_date) : t(''),
    );
  }
  else {
    $form['release_date'] = array(
      '#title' => t('Movie Release Date'),
      '#description' => t('Day the film was released (Format: August 07, 2010)'),
      '#type' => 'textfield',
      '#size' => 50,
      '#maxlength' => 255,
      '#default_value' => ($movie_wishlist->release_date) ? date("F d, Y", $movie_wishlist->release_date) : t(''),
    );
  }

  $form['runtime'] = array(
    '#title' => t('Movie Runtime'),
    '#description' => t('Runtime, in minutes, of film (e.g. 122)'),
    '#type' => 'textfield',
    '#size' => 11,
    '#maxlength' => 11,
    '#default_value' => $movie_wishlist->runtime,
  );

  $form['active'] = array(
    '#type' => 'hidden',
    '#default_value' => $movie_wishlist->active,
  );

  //If this is an existing entry, elaborate a little.
  if($movie_wishlist_id) {
    $form['active']['#type'] = 'checkbox';
    $form['active']['#title'] = t('Active');

    $form['added_to_movies'] = array(
      '#title' => t('Added to Movies'),
      '#type' => 'checkbox',
      '#default_value' => $movie_wishlist->added_to_movies,
      '#disabled' => TRUE,
    );

    $form['created'] = array(
      '#title' => t('Created'),
      '#type' => 'textfield',
      '#size' => 50,
      '#maxlength' => 50,
      '#default_value' => ($movie_wishlist->created) ? format_date($movie_wishlist->created) : t('never'),
      '#disabled' => TRUE,
    );

    $form['updated'] = array(
      '#title' => t('Updated'),
      '#type' => 'textfield',
      '#size' => 50,
      '#maxlength' => 50,
      '#default_value' => ($movie_wishlist->updated) ? format_date($movie_wishlist->updated) : t('never'),
      '#disabled' => TRUE,
    );
  }

  $form['movie_wishlist_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}


/**
 * movies_wishlist_user_add_validate()
 *
 * hook_validate
 *
 * Validate movie wishlist record before attempting to save
 */
function movies_wishlist_user_add_validate($form, &$form_state) {
  //If the release date was provided, ensure it's in a proper format
  if($form_state['values']['release_date'] != '') {
    if(!strtotime($form_state['values']['release_date'])) {
      form_set_error('release_date', t('Invalid release date, please use format: August 07, 2010'));
    }
  }

  //If Runtime was provided, ensure it's numeric
  if(!is_numeric($form_state['values']['runtime'])) {
    form_set_error('runtime', t('Invalid runtime!  Only numbers can be used here, representing runtime in minutes of the film (e.g. 123)'));
  }

  //If the IMDB # was provided, ensure it follows loose standards of what we 
  //know of these ids.
  if($form_state['values']['imdb_id'] != '') {
    if(!preg_match('/^tt(\d+)$/', $form_state['values']['imdb_id'])) {
      form_set_error('imdb_id', t('Invalid IMDB #, please use format: tt1375666'));
    }
  }
}


/**
 * movies_wishlist_user_add_submit()
 *
 * hook_submit
 *
 * Add or update the movie wishlist record, tieing into play the format and
 * genre(s) specified.
 */
function movies_wishlist_user_add_submit($form, &$form_state) {
  //Attempt to load the existing movie record
  $movie = new movie_wishlist_extra();
  $movie->movie_wishlist_id = $form_state['values']['movie_wishlist_id_hidden'];
  $movie->find();

  //Attempt to load the specified format
  $movie_format = new movie_format_extra();
  $movie_format->movie_format = $form_state['values']['movie_format'];

  //If the format has a value...
  if($form_state['values']['movie_format'] != '') {
    $movie_format->find_by_format();

    //If it doesn't exist, create it
    if(!$movie_format->movie_format_id) {
      $movie_format->created = mktime();
      $movie_format->update();
    }
  }

  //Build the rest of the movie record, based on details filled in by user.
  $movie->movie_format_id = $movie_format->movie_format_id;
  $movie->uid = $form_state['values']['uid'];
  $movie->imdb_id = $form_state['values']['imdb_id'];
  $movie->title = $form_state['values']['title'];
  $movie->release_date = strtotime($form_state['values']['release_date']);
  $movie->runtime = $form_state['values']['runtime'];
  $movie->active = $form_state['values']['active'];

  //If movie already exists
  if($movie->movie_wishlist_id) {
    //Set update time to now
    $movie->updated = mktime();
  }
  //Otherwise...
  else {
    //Set created time to now
    $movie->created = mktime();
  }

  //Update movie record
  $movie->update();

  //Disable any genre listings for this film
  $movie_genre_link = new movie_wishlist_genre_link_extra();
  $movie_genre_link->movie_wishlist_id = $movie->movie_wishlist_id;
  $movie_genre_link->disable_all_by_movie();

  //If movie genres were provided
  if($form_state['values']['movie_genres']) {
    //Break them apart into single listings
    $movie_genres = split(',', $form_state['values']['movie_genres']);

    //If there are movie genres
    if(count($movie_genres)) {
      //Go through each
      foreach($movie_genres as $this_genre) {
        //Get rid of any prefix spacing
        $this_genre = preg_replace('/^(\s+)/', '', $this_genre);

        //Get rid of any suffix spacing
        $this_genre = preg_replace('/(\s+)$/', '', $this_genre);

        //See if we can find this genre
        $movie_genre = new movie_genre_extra();
        $movie_genre->movie_genre = $this_genre;
        $movie_genre->find_by_genre();

        //It doesn't exist.  Create it
        if(!$movie_genre->movie_genre_id) {
          $movie_genre->created = mktime();
          $movie_genre->update();
        }

        //Check to see if an existing movie-genre link exists
        $movie_genre_link = new movie_wishlist_genre_link_extra();
        $movie_genre_link->movie_wishlist_id = $movie->movie_wishlist_id;
        $movie_genre_link->movie_genre_id = $movie_genre->movie_genre_id;
        $movie_genre_link->find_by_details();
        
        //One exists
        if($movie_genre_link->movie_wishlist_genre_link_id) {
          //Make it active and set the updated time
          $movie_genre_link->active = 1;
          $movie_genre_link->updated = mktime();
        }
        //None exists
        else {
          //Make it active and set the created time
          $movie_genre_link->active = 1;
          $movie_genre_link->created = mktime();
        }

        //Update the record
        $movie_genre_link->update();
      }
    }
  }

  //Send user back to their movie listings page
  return drupal_goto('user/' . $form_state['values']['uid'] . '/movies/wishlist');
}


/**
 * movies_wishlist_user_add_to_movies()
 *
 * Confirm form for converting wishlist films into movies
 */
function movies_wishlist_user_add_to_movies(&$form_state, $account, $movie_wishlist_id = 0) {
  //Make the system think that we're on the main wishlist page, to ensure that
  //page tabs remain in place.
  menu_set_active_item('user/' . $account->uid . '/movies/wishlist');

  //Load user object for this user
  global $user;

  //If this user shouldn't be here, show them access denied
  if(!user_edit_access($account)) {
    return drupal_access_denied();
  }

  //Set the page title with what data is available.
  $title = isset($account->title) ? $account->title : $account->name;
  drupal_set_title(check_plain($title));

  $movie_wishlist = new movie_wishlist_extra();
  $movie_wishlist->movie_wishlist_id = $movie_wishlist_id;
  $movie_wishlist->find();

  $form['movie_wishlist_id'] = array(
    '#type' => 'hidden',
    '#value' => $movie_wishlist->movie_wishlist_id,
    '#suffix' => '<h2 class="title">Are you sure you wish to Add  ' . $movie_wishlist->title . ' to your movie list?</h2>',
  );

  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $account->uid,
  );

  $form['convert'] = array(
    '#type' => 'submit',
    '#value' => t('Convert to Movies'),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel Request'),
  );
  
  return $form;
}


/**
 * movies_wishlist_user_add_to_movies_submit()
 *
 * hook_submit
 *
 * Clones data from wishlist entry into a new movie
 */
function movies_wishlist_user_add_to_movies_submit($form, &$form_state) {
  if($form_state['values']['op'] == t('Cancel Request')) {
    return drupal_goto('user/' . $form_state['values']['uid'] . '/movies/wishlist');
  }

  //Attempt to load the existing movie record
  $movie_wishlist = new movie_wishlist_extra();
  $movie_wishlist->movie_wishlist_id = $form_state['values']['movie_wishlist_id'];
  $movie_wishlist->find();

  //If we have no title, we'll assume the id was invalid and won't continue
  if($movie_wishlist->title == '') {
    drupal_set_message('This does not appear to be a valid film.');
    return drupal_goto('user/' . $form_state['values']['uid'] . '/movies/wishlist');
  }

  //If the record is inactive, we won't continue
  if(!$movie_wishlist->active) {
    drupal_set_message('This film is not active, therefore I will not convert it.  Please activate the film first, if you wish to convert it.');
    return drupal_goto('user/' . $form_state['values']['uid'] . '/movies/wishlist');
  }

  //If the record has already been converted, we won't continue
  if($movie_wishlist->added_to_movies) {
    drupal_set_message('This film has already been converted to your list, according to our database.');
    return drupal_goto('user/' . $form_state['values']['uid'] . '/movies/wishlist');
  }

  //Attempt to load the existing movie record
  $movie = new movie_extra();

  //Build the rest of the movie record, based on details filled in by user.
  $movie->movie_format_id = $movie_wishlist->movie_format_id;
  $movie->uid = $movie_wishlist->uid;
  $movie->imdb_id = $movie_wishlist->imdb_id;
  $movie->title = $movie_wishlist->title;
  $movie->release_date = $movie_wishlist->release_date;
  $movie->runtime = $movie_wishlist->runtime;
  $movie->active = 1;
  $movie->created = mktime();

  //Update movie record
  $movie->update();

  //Load all genres attached to this film
  $movie_genre_link = new movie_wishlist_genre_link_extra();
  $movie_genre_links = $movie_genre_link->find_many(' movie_wishlist_id = ' . $movie_wishlist->movie_wishlist_id . ' AND active ');

  //If movie genres were provided
  if(count($movie_genre_links)) {
    //Go through each
    foreach($movie_genre_links as $this_genre_link) {
      //Add a new movie-genre link
      $movie_genre_link = new movie_genre_link_extra();
      $movie_genre_link->movie_id = $movie->movie_id;
      $movie_genre_link->movie_genre_id = $this_genre_link->movie_genre_id;
      $movie_genre_link->find_by_details();
      $movie_genre_link->active = 1;
      $movie_genre_link->created = mktime();

      //Update the record
      $movie_genre_link->update();
    }
  }

  $movie_wishlist->added_to_movies = 1;
  $movie_wishlist->updated = mktime();
  $movie_wishlist->update();

  drupal_set_message('Film successfully moved to movies list');
  drupal_goto('user/' . $form_state['values']['uid'] . '/movies/wishlist');
}


/**
 * movies_user_fix_conflict()
 *
 * Confirm form for fixing conflicted film
 */
function movies_user_fix_conflict(&$form_state, $account, $movie_wishlist_id = 0) {
  //Make the system think that we're on the main conflicts page, to ensure that
  //page tabs remain in place.
  menu_set_active_item('user/' . $account->uid . '/movies/conflicts');

  //Load user object for this user
  global $user;

  //If this user shouldn't be here, show them access denied
  if(!user_edit_access($account)) {
    return drupal_access_denied();
  }

  //Set the page title with what data is available.
  $title = isset($account->title) ? $account->title : $account->name;
  drupal_set_title(check_plain($title));

  $movie_wishlist = new movie_wishlist_extra();
  $movie_wishlist->movie_wishlist_id = $movie_wishlist_id;
  $movie_wishlist->find();

  $form['movie_wishlist_id'] = array(
    '#type' => 'hidden',
    '#value' => $movie_wishlist->movie_wishlist_id,
    '#suffix' => '<h2 class="title">Are you sure you wish to fix the conflict for ' . $movie_wishlist->title . '?</h2>',
  );

  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $account->uid,
  );

  $form['convert'] = array(
    '#type' => 'submit',
    '#value' => t('Fix Conflict'),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel Request'),
  );
  
  return $form;
}


/**
 * movies_user_fix_conflict_submit()
 *
 * hook_submit
 *
 * If conflict is determined to still exist (hey, things can change 
 * unexpectedly) simply note in the wishlist that this film has been added to
 * the movies table.
 */
function movies_user_fix_conflict_submit($form, &$form_state) {
  if($form_state['values']['op'] == t('Cancel Request')) {
    return drupal_goto('user/' . $form_state['values']['uid'] . '/movies/conflicts');
  }

  //Attempt to load the existing movie record
  $movie_wishlist = new movie_wishlist_extra();
  $movie_wishlist->movie_wishlist_id = $form_state['values']['movie_wishlist_id'];
  $movie_wishlist->find();

  //If we have no title, we'll assume the id was invalid and won't continue
  if($movie_wishlist->title == '') {
    drupal_set_message('This does not appear to be a valid film.');
    return drupal_goto('user/' . $form_state['values']['uid'] . '/movies/conflicts');
  }

  //If the record is inactive, we won't continue
  if(!$movie_wishlist->active) {
    drupal_set_message('This film is not active, therefore I\'m not sure how to handle it.  Doing nothing instead.');
    return drupal_goto('user/' . $form_state['values']['uid'] . '/movies/conflicts');
  }

  //If the record has already been converted, we won't continue
  if($movie_wishlist->added_to_movies) {
    drupal_set_message('This conflict appears to have resolved itself.  Nothing left to do here.');
    return drupal_goto('user/' . $form_state['values']['uid'] . '/movies/conflicts');
  }

  $movie_wishlist->added_to_movies = 1;
  $movie_wishlist->updated = mktime();
  $movie_wishlist->update();

  drupal_set_message('Conflict issue should be resolved for ' . $movie_wishlist->title);
  drupal_goto('user/' . $form_state['values']['uid'] . '/movies/conflicts');
}
